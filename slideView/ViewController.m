//
//  ViewController.m
//  slideView
//
//  Created by Click Labs133 on 10/23/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){UIImageView *backImage;}
@property (strong, nonatomic) IBOutlet UIButton *menuButton;
@end

@implementation ViewController

UIView *view1;

@synthesize menuButton;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    backImage=[[UIImageView alloc]init];
    backImage.frame=CGRectMake(0,0, 450, 600);
    [UIImage imageNamed:@"/Users/clicklabs133/Desktop/poojaProject/slideView/slideView/thumb_gorgeous-chevrolet-silverado.jpg"];
    [self.view addSubview:backImage];
    view1=[[UIView alloc]init];
    view1.frame=CGRectMake(-300, 170, 170, 400);
    view1.backgroundColor=[UIColor grayColor];

    [self.view addSubview:view1];
}
- (IBAction)menuButtonPressed:(id)sender {
    //view1.frame.origin=[100,100];
    
  /*  [UIView animateWithDuration:1.0 delay:0.5 options:UIViewAnimationCurveLinear animations:^{view1.frame=CGRectMake(0, 100, 170, 400);} completion:<#^(BOOL finished)completion#>];*/
    [UIView animateWithDuration:1.0
                          delay:1.0
                        options: UIViewAnimationCurveEaseOut
                     animations:^{
                         view1.frame=CGRectMake(0, 170, 170, 400);
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Done!");
                     }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
